﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqExe1
{
    public partial class Form1 : Form
    {
        List<Article> arts = new List<Article>();
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            string categorie = txtCtg.Text.ToLower().Trim();

            List<Article> req = 
                 arts
                .Where(x => x.Ctg.ToLower() == categorie )
                .ToList();
            int nb = req.Count;
            double moy = nb>0?req.Average(x => x.Pu):0;

            grd.DataSource = req;
            lbMoy.Text = moy.ToString();
            lbNb.Text = nb.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            arts.Add(new Article() { Code = 1, Libelle = "Pain", Ctg = "alm", Pu = 2.20 });
            arts.Add(new Article() { Code = 2, Libelle = "Pomme", Ctg = "Alm", Pu = 18.56 });
            arts.Add(new Article() { Code = 3, Libelle = "Maths", Ctg = "liv", Pu = 100 });
            arts.Add(new Article() { Code = 4, Libelle = "Huile", Ctg = "aLM", Pu = 15 });
            arts.Add(new Article() { Code = 5, Libelle = "Chimie", Ctg = "liv", Pu = 81 });
            arts.Add(new Article() { Code = 6, Libelle = "Stylo", Ctg = "frn", Pu = 5 });
            grd.DataSource = arts;
        }
    }
}
