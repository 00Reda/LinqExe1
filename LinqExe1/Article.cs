﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExe1
{
   public class Article
    {
        public Article()
        {

        }

        public int Code { get; set; }
        public string Libelle { get; set; }
        public double Pu { get; set; }
        public string Ctg { get; set; }
    }
}
