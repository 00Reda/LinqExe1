﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExe2
{
    public class Mouvement
    {
        public int num { get; set; }
        public int codeArt { get; set; }
        public double quantite { get; set; }
        public char nature { get; set; }

        public Mouvement()
        {

        }

        public Mouvement(int num, int codeArticle, double quantite, char nature)
        {
            this.num = num;
            this.codeArt = codeArticle;
            this.quantite = quantite;
            this.nature = nature;
        }
    }
    public class Article
    {
        public int CodeArt { get; set; }
        public string Libelle { get; set; }
        public double PU { get; set; }
        public Article()
        {

        }

        public Article(int codeArt, string libelle, double pU)
        {
            CodeArt = codeArt;
            Libelle = libelle;
            PU = pU;
        }
    }
    public class DB
    {
        public List<Mouvement> Mvts { get; set; }
        public List<Article> Arts { get; set; }
        public DB()
        {
            Mvts = new List<Mouvement>();

            Mvts.Add(new Mouvement(1, 1, 100, 'E'));
            Mvts.Add(new Mouvement(2, 2, 50, 'E'));
            Mvts.Add(new Mouvement(3, 1, 70, 'S'));
            Mvts.Add(new Mouvement(4, 3, 10, 'E'));
            Mvts.Add(new Mouvement(5, 1, 10, 'S'));
            Mvts.Add(new Mouvement(6, 4, 20, 'E'));
            Mvts.Add(new Mouvement(7, 2, 30, 'S'));

            Arts = new List<Article>();
            Arts.Add(new Article(1, "Pain", 2));
            Arts.Add(new Article(2, "Lait", 2));
            Arts.Add(new Article(3, "Sucre", 2));
            Arts.Add(new Article(4, "The", 2));
            Arts.Add(new Article(5, "Pomme", 2));

        }
    }
}
