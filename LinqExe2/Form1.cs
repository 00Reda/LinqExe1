﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqExe2
{
    public partial class Form1 : Form
    {
        DB db = new DB();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAfficher_Click(object sender, EventArgs e)
        {
            dynamic res=null; 
            if (rd1.Checked)
            {
              res = db.Mvts
                               .GroupBy(x => x.codeArt)
                               .Select(g =>
                           new {
                               g.Key,
                               Nb = g.Count(),
                               Qte = g.Sum(m => SignQte(m))
                           }).ToList();
            }
            if (rd2.Checked)
            {
                var ArtCmd = db.Mvts.Select(x => x.codeArt).Distinct().ToList();
                res = db.Arts.Where(x => !ArtCmd.Contains(x.CodeArt)).ToList();
            }
            if (rd3.Checked)
            {
                int maxNb = db.Mvts
                                 .GroupBy(x => x.codeArt)
                                 .Max(g => g.Count());
                res = db.Mvts
                                .GroupBy(x => x.codeArt)
                                .Where(g => g.Count() == maxNb).ToList();


            }
           

            grd.DataSource = res;
        }

        private double SignQte(Mouvement m)
        {
            return m.nature == 'E' ? m.quantite : -m.quantite;
        }
    }
}
